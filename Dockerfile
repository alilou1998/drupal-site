FROM drupal:8.9.13

WORKDIR /opt/drupal/web

COPY ./web .

RUN chown www-data:www-data modules sites themes  -R

EXPOSE 80
